#include <stdio.h>

int main()
{
  printf("%d \n", sizeof("A")); //2
  printf("%d \n", sizeof('A')); //4

  printf("%d \n", sizeof("ABCDEFG")); //8
  printf("%d \n", sizeof('ABCDEFG')); //4

  printf("%d \n", sizeof("abcd"));  //5
  printf("%d \n", sizeof('abcd'));  //4

  printf("%d \n", sizeof("abcdEFG"));  //8
  printf("%d \n", sizeof('abcdEFG'));  //4
  printf("%d \n", sizeof('abcsfdsdkldEFG'));  //4

  // 문자열을 ""와 ''로 입력하였을 경우 할당받는 크기가 다르다.
  // ''로 입력받은 문자열은 무조건 크기가 4바이트이다. 그러나 ""로 입력받은 문자열은 길이에 따라 크기가 달라진다.
  // ''로 입력받은 문자열의 크기는 어떻게 정해지는건지, 왜 ""로 입력받은 문자열은 무조건 크기가 4바이트인지 알아볼 것.

  return 0;
}
