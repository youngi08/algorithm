#include <cstdio>
#include <queue>
using namespace std;

queue<int> q;
int button[1000001];
int visited[1000001];
int S, G, F, U, D;
//F=스타트링트 건물 층, S=강호가 지금 있는 층, G=이동할 층, U=위로 이동, D=아래로 이동
//G층에 도착하려면, 버튼을 적어도 몇 번 눌러야 하나?

void bfs()
{

  while(!q.empty()){
    int next = q.front();

    q.pop();
    if(next==G) return;

    //이동할 위치가 건물 층보다 작고 아직 방문하지 않았으면
    if(next + U <= F && visited[next + U] == 0){
      //이동한 위치
      button[next + U] = button[next]+1;
      q.push(next + U);
      visited[next + U] = 1;
    }

    if(next - D >= 1 && visited[next - D] == 0){
      button[next - D] = button[next]+1;
      q.push(next - D);
      visited[next - D] = 1;
    }
  }
}

int main()
{
    scanf("%d %d %d %d %d", &F, &S, &G, &U, &D);

    if(S == G){
      printf("0\n");
      return 0;
    }

    visited[S] = 1;
    button[S] = 0;

    q.push(S);
    bfs();

    for(int i=0; i<=F; i++){
      printf("%d %d\n", visited[i], button[i]);
    }

    if(visited[G] == 0) printf("use the stairs\n");
    else printf("%d\n", button[G]);

    return 0;
}


/*
#include <cstdio>
using namespace std;

int main()
{
  int F, S, G, U, D;
  //F=스타트링트 건물 층, S=강호가 지금 있는 층, G=이동할 층, U=위로 이동, D=아래로 이동
  //G층에 도착하려면, 버튼을 적어도 몇 번 눌러야 하나?

  scanf("%d %d %d %d %d", &F, &S, &G, &U, &D);
  int res;

  for(int i=0; i<F; i++){
    res = S + i*U;

    if(res == G){
      printf("%d\n", i);
      return 0;
    }
    else if(res > G){
      for(int j=0; j<F; j++){
        res = res - j*D;
        if(res == G){
          printf("%d\n", i+j);
          return 0;
        }
      }
    }
  }

  printf("use the stairs\n");

  return 0;
}
*/
