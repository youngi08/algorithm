#include <iostream>
using namespace std;

string str;
bool checkcpp()
{
  for(int i=1; i<str.size(); i++){
    //_ 또는 소문자로 이루어 질 때만 c++로 인식
    if(str[i] == '_'){
      //_가 연속으로 나오면 에러
      if(str[i+1] == '_') return false;
    }
    else if(str[i] >= 'a' && str[i] <= 'z'){
      continue;
    }
    else return false;
  }
  return true;
}

bool checkjava()
{
  for(int i=1; i<str.size(); i++){
    //대문자 또는 소문자로 이루어 질 때만 자바로 인식
    if((str[i] >= 'A' && str[i] <= 'Z') || (str[i] >= 'a' && str[i] <= 'z')){
      continue;
    }
    else return false;
  }
  return true;
}

int main()
{
  cin >> str;

  //첫 글자가 _ 이거나, 대문자 이거나, 마지막 글자가 _ 이면 에러
  if(str[0] == '_' || (str[0]>='A' && str[0] <='Z') || str[str.size()-1] == '_'){
    cout << "Error!" << endl;
    return 0;
  }

  bool isCpp = checkcpp();
  bool isJava = checkjava();

  string res;
  if(!isCpp && !isJava){
    cout << "Error!" << endl;
    return 0;
  }
  else if(isCpp){
    for(int i=0; i<str.size(); i++){
      if(str[i] == '_'){
        res.push_back(str[i+1]-32);
        i++;
      }
      else res.push_back(str[i]);
    }
  }
  else if(isJava){
    for(int i=0; i<str.size(); i++){
      if(str[i]>='A' && str[i] <= 'Z'){
        res.push_back('_');
        res.push_back(str[i]+32);
      }
      else res.push_back(str[i]);
    }
  }
  cout << res << endl;

  return 0;
}
