//리스트
/*
L 커서를 왼쪽으로 한 칸 옮김 (커서가 문장의 맨 앞이면 무시됨)
D	커서를 오른쪽으로 한 칸 옮김 (커서가 문장의 맨 뒤이면 무시됨)
B	커서 왼쪽에 있는 문자를 삭제함 (커서가 문장의 맨 앞이면 무시됨)
삭제로 인해 커서는 한 칸 왼쪽으로 이동한 것처럼 나타나지만, 실제로 커서의 오른쪽에 있던 문자는 그대로임
P $	$라는 문자를 커서 왼쪽에 추가함
*/

#include <iostream>
#include <list>
#include <string>
using namespace std;

int main() {
    string s;
    cin >> s;

    list<char> editor(s.begin(), s.end());
    auto cursor = editor.end();

    int n;
    cin >> n;

    while(n--) {
        char cmd;
        cin >> cmd;
        if(cmd == 'L') {
            if(cursor != editor.begin()) {
                --cursor;
            }
        } else if(cmd == 'D') {
            if(cursor != editor.end()) {
                ++cursor;
            }
        } else if(cmd == 'B') {
            if(cursor != editor.begin()) {
                auto temp = cursor;
                --cursor;
                editor.erase(cursor);
                cursor = temp;
            }
        } else if(cmd == 'P') {
            char c;
            cin >> c;
            editor.insert(cursor, c);
        }
    }

    for(char c : editor) {
        cout << c;
    }
    cout << '\n';
  }
