#include <iostream>
using namespace std;

int main()
{
  int N, M, K;
  cin >> N >> M >> K;

  int wNum=0, group=0;

  wNum = N/2;

  if(M < wNum){
    if(K < M){
      group = K;
    }
    else{
      group = M;
    }
  }
  else{
    if(K < wNum){
      group = K;
    }
    else{
      group = wNum;
    }
  }

  cout << group << endl;

  return 0;
}
