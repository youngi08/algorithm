#include <iostream>
#include <cmath>
#include <algorithm>
using namespace std;

int button[10] = {0};

int possible(int n)
{
   if(n == 0) {
      if (button[0] != 1) return 1;   // 고장 안났다면
      else return -1;               // 고장 났다면
   }

   int len = 0;
   while (n > 0){
    if(button[n % 10] == 1) return -1;
    else{
      n /= 10;
      len++;
    }
   }
   return len;
}

int main()
{
  int N, M;
  cin >> N >> M;
  int now = 100;

  //고장난 버튼 체크
  for(int i=0; i<M; i++){
    int tmp;
    cin >> tmp;
    button[tmp] = 1;
  }

  int cur = abs(now - N);
  int res = 0, press = 0;
  int minVal = 1000000;

  for(int i=0; i<1000000; i++) {
      press = possible(i);

      if(press > 0) {
         res = abs(N - i) + press;
         minVal = min(res, minVal);
      }
   }

   int ret = min(minVal, cur);
   cout << ret << endl;

   return 0;
}
