#include <iostream>
#include <cmath>
using namespace std;

int map[6][6] = {0};

int main()
{
  int num = 36;

  string str = "11";
  for(int i=0; i<num; i++){
    string tmp = str;
    int tx, ty;
    tx = tmp[0]-'A'+1;
    ty = tmp[1]-'0';

    cin >> str;
    int x, y;
    x = str[0]-'A'+1;
    y = str[1]-'0';

    string start;
    int sx, sy;
    if(tmp == "11"){
      start = str;
      sx = start[0]-'A'+1;
      sy = start[1]-'0';
      map[sx][sy] = 1;
      continue;
    }

    //방문체크
    map[x][y]++;
    //cout << "tmp=" << tmp <<"str="<< str << endl;

    //나이트의 이동인지 검사
    if(abs(tx-x) <3 && abs(ty-y)<3 && abs(tx-x) + abs(ty-y) > 3){
      //cout << abs(tx-x) <<" "<< abs(ty-y) << endl;
      cout << "Invalid" << endl;
      return 0;
    }

    //마지막 좌표에서 시작좌표로 갈 수 있는지 검사
    if(i == num-1){
      int lx = x;
      int ly = y;
      if(abs(sx-lx) <3 && abs(sy-ly)<3 && abs(sx-lx) + abs(sy-ly) > 3){
        //cout << sx <<" "<< lx<<" " << sy<<" " << ly << endl;
        cout << "Invalid" << endl;
        return 0;
      }
    }
  }

  for(int i=1; i<=6; i++){
    for(int j=1; j<=6; j++){
      //cout << map[i][i] << " ";
      if(map[i][j] == 0 || map[i][j] > 1){
        cout << "Invalid" << endl;
        return 0;
      }
    }
      //cout << endl;
  }

  cout << "Valid" << endl;
  return 0;
}
