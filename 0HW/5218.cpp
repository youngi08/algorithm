#include <iostream>
#include <cstdio>
using namespace std;

int main()
{
  int num;
  cin >> num;

  for(int i=0; i<num; i++){
    string str, com;
    cin >> str >> com;

    printf("Distances: ");
    for(int j=0; j<str.size(); j++){
      int tmp = 0;
      if(str[j] > com[j]){
        tmp = com[j]+26 -str[j];
        printf("%d ", tmp);
      }
      else{
        tmp = (com[j]-str[j]);
        printf("%d ", tmp);
      }
    }
    printf("\n");
  }
  return 0;
}
