//BFS
//7569.cpp 토마토
#include<iostream>
#include <queue>
using namespace std;

int box[100][100][100]={0};
int day[100][100][100]={0};

int dx[6] = {0, 1, 0, -1, 0, 0};
int dy[6] = {1, 0, -1, 0, 0, 0};
int dh[6] = {0, 0, 0, 0, 1, -1};
int N, M, H;  //가로, 세로, 높이

queue <pair<pair<int, int>, int>> tomato;

bool inRange(int x, int y, int h)
{
  return(x>=0 && x<M && y>=0 && y<N && h>=0 && h<H);
}

void bfs()
{
  while (!tomato.empty()){
    int x = tomato.front().first.first;
    int y = tomato.front().first.second;
    int h = tomato.front().second;

    tomato.pop();

    for(int i = 0; i < 6; i++) {
      int nx = x + dx[i];
      int ny = y + dy[i];
      int nh = h + dh[i];

      if(inRange(nx, ny, nh) && box[ny][nx][nh] == 0) {
        box[ny][nx][nh] = 1;
        day[ny][nx][nh] = day[y][x][h] + 1;
        tomato.push(make_pair(make_pair(nx, ny), nh));
      }
    }
  }
}

int check(){
  int ans = 0;

  for(int k = 0; k < H; k++){
    for(int i = 0; i < N; i++){
      for(int j = 0; j < M; j++){
        if(ans < day[i][j][k]) ans = day[i][j][k];
        if(box[i][j][k] == 0 && day[i][j][k] == 0) return -1;
      }
    }
  }
  return ans;
}

int main()
{
  cin >> M >> N >> H;

  for(int k = 0; k < H; k++){
    for(int i = 0; i < N; i++){
      for(int j = 0; j < M; j++){
        cin >> box[i][j][k];

        if(box[i][j][k] == 1) tomato.push(make_pair(make_pair(j, i), k));
      }
    }
  }

  bfs();

  int day = check();
  cout << day << endl;

  return 0;
}
