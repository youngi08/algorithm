#include <iostream>
#include <string>

using namespace std;

int main()
{
  int T;
  string input;
  int check =0;

  cin >> T;
  for(int i=0; i<T; i++){
    cin >> input;

    int one=0, two=0, three=0, count=0;
    for(int j=0; j<input.size(); j++){
      if(input[j] == '('){
        one++;
      }
      if(input[j] == '{'){
        two++;
      }
      if(input[j] == '['){
        three++;
      }
      else{
        if(one > 0 || two >0 || three >0){
          count++;
        }
      }
    }
  }

  cout << check << endl;

  return 0;
}
