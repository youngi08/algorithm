//6359.cpp 만취한 상범
//DP

#include <iostream>
using namespace std;
int room[101]={0};

void calculate(int num){
  for(int i=1; i<=num; i++){
    for(int j=i; j<=num; j++){
      if(j%i == 0){
        if(room[j] == 0){
          room[j] = 1;
        }
        else
          room[j] = 0;
        }
      }
    }
  }
}

int main()
{
    int T;
    cin >> T;

    for(int i=0; i<T; i++){
        int input=0;
        cin >> input;

        for(int k=1; k<=input; k++){
            room[k] = 0;
        }

        calculate(input);

        int count =0;
        for(int j=1; j<=input; j++){
            if(room[j] == 1){
                count++;
            }
        }
        cout << count << endl;
    }

    return 0;
}
