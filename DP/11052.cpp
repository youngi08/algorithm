/*
#include <iostream>
#include <algorithm>
using namespace std;

int dp[1000]={0};

int main()
{
  int N;
  cin >> N;

  for(int i=1; i<=N; i++){
    cin >> dp[i];

    for(int j=1; j<=i; j++){
      if(dp[i] + dp[i+j] > dp[i])
        dp[i] = dp[i] + dp[i+j];
    }
  }
  cout << dp[N] << endl;

  return 0;
}
*/

#include <iostream>
#include <algorithm>
using namespace std;

int dp[1000]={0};
int cost[1000] = {0};

int main()
{
  int N;
  cin >> N;

  int res = 0;
  for(int i=1; i<=N; i++){
    cin >> cost[i];
    for(int j=1; j<=i; j++){
      dp[i] = max(dp[i], dp[i-j]+cost[j]);
    }
  }
  cout << dp[N] << endl;

  return 0;
}
