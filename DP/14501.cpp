//14501.cpp 퇴사
//삼성 SW 역량 테스트

#include <iostream>
using namespace std;

int day[100]={0};
int fee[100]={0};
int total=0;
int N;
int sum=0;

int solve(int dday, int next){
  //cout << i << endl;
  //cout << dday << endl;

  total += dday;
  cout << total << endl;

  if(total < N){
    sum += fee[next];
    next = dday+1;
    solve(day[next], next);
  }
  return 0;
}

int main()
{
  int T;  //테스트 케이스 갯수
  cin >> T;

  for(int i=0; i<T; i++){

    cin >> N;
    for(int i=0; i<N; i++){
      cin >> day[i] >> fee[i];
      solve(day[i], i);
    }

    //cout << sum << endl;

  }



}
