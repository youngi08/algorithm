#include <iostream>
#include <cstring>
using namespace std;

int dp[11];

int main()
{
  int T;
  cin >> T;

  while(T--){
    memset(dp, 0, sizeof(dp));

    dp[0] = 1;
    dp[1] = 1;
    dp[2] = 2;

    int testc;
    cin >> testc;

    for(int i=3; i<=testc; i++){
      dp[i] = dp[i-1] + dp[i-2] + dp[i-3];
    }
    cout<< dp[testc] << endl;
  }

  return 0;
}
