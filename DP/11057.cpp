//dp
//11057.cpp 오르막수

#include <iostream>
#define mod 10007
using namespace std;

int dp[1000][10]={0};

int main()
{
  int num;
  cin >> num;

  //1자리 수일때
  for(int i=0; i<=9; i++) dp[1][i] = 1;

  //2자리 수일때~ num자리 수일때
  for(int i=2; i<=num; i++){
    for(int j=0; j<=9; j++){
      for(int k=0; k<=j; k++){
        dp[i][j] = dp[i-1][k];
        dp[i][j] %= mod;
      }
    }
  }

  long long ans = 0;
  for(int i=0; i<=9; i++) ans += dp[num][i];

  cout << ans%mod << endl;

  return 0;
}
