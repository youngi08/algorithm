#include <iostream>
#include <algorithm>
using namespace std;

long long dp[100000][3] = {0};
long long a[100000][3] = {0};

int main()
{
  int T;
  cin >> T;
  for(int i=0; i<T; i++){
    int n;
    cin >> n;

    for(int j=0; j<2; j++){
      for(int i=1; i<=n; i++){
        cin >> a[i][j];
      }
    }

    for(int i=1; i<=n; i++) {
      dp[i][0] = max(max(dp[i-1][0], dp[i-1][1]), dp[i-1][2]);
      dp[i][1] = max(dp[i-1][0], dp[i-1][2]) + a[i][0];
      dp[i][2] = max(dp[i-1][0], dp[i-1][1]) + a[i][1];
    }

    long long res = 0;
    for (int i=1; i<=n; i++){
     res = max(max(dp[i][1], dp[i][2]), dp[i][0]);

    }
    cout << res << endl;
  }
  return 0;
}
