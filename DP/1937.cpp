#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;

int map[501][501]={0};
int dp[501][501]={0};
int dy[4] = {1, 0, -1, 0};
int dx[4] = {0, 1, 0, -1};
int n;

bool inRange(int x,int y){
    return (x >= 0 && x < n && y >= 0 && y < n);
}

int dfs(int x, int y){
  if(dp[x][y] == 0){
    dp[x][y] = 1;

    for(int i = 0; i < 4; i++){
      int nx = x + dx[i];
      int ny = y + dy[i];

      if(inRange(nx,ny) && map[nx][ny] > map[x][y])
        dp[x][y] = max(dp[x][y], dfs(nx,ny)+1);
    }
  }
  return dp[x][y];
}

int main(){
  cin >> n;

  for(int i = 0; i < n; i++){
    for(int j = 0; j < n; j++){
      cin >> map[i][j];
    }
  }

  int res = 0;
  for(int i = 0; i < n; i++){
    for(int j = 0; j < n; j++){
        res = max(res, dfs(i,j));
    }
  }
  cout << res << endl;

  return 0;
}
