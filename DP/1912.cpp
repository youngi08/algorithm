#include <cstdio>
#include <vector>
#define max(x,y) (x)>=(y)?(x):(y)

using namespace std;

int main(int argc, const char * argv[])
{
    int num, input;
    long res;

    std::vector<int> nums;

    scanf("%d", &num);

    for(int i=0; i< num; i++){
        scanf("%d", &input);
        nums.push_back(input);
    }
    res = nums[0];

    for (int i = 1; i < num; i++){
        if (nums[i - 1] > 0)
            nums[i] = nums[i - 1] + nums[i];
        res = max(res, nums[i]);
    }

    printf("%ld\n", res);

    return 0;
}
