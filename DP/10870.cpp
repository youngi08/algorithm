#include <iostream>
using namespace std;

int v[20] = {0};

int main()
{
  int num;
  cin >> num;

  v[0] = 0;
  v[1] = 1;
  for(int i=2; i<=num; i++)
    v[i] = v[i-1] + v[i-2];

  cout << v[num] << endl;
  return 0;
}
