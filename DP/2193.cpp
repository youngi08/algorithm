//DP
//2193.cpp 이친수
/*
이친수는 0으로 시작하지 않는다.
이친수에서는 1이 두 번 연속으로 나타나지 않는다. 즉, 11을 부분 문자열로 갖지 않는다.
*/
/*
#include <iostream>
using namespace std;

long long int dp[90]={0};
int main()
{
  long long int N; //N자리 이친수
  cin >> N;

  dp[0] = 0;
  dp[1] = 1;
  for(int i=2; i<=N; i++){
    dp[i] = dp[i-1] + dp[i-2];
  }
  cout << dp[N] << endl;

  return 0;
}
*/

#include <iostream>
using namespace std;

long long int dp[90][2]={0};
int main()
{
  long long int N; //N자리 이친수
  cin >> N;

  dp[1][0] = 1;
  dp[1][1] = 1;

  for(int i=2; i <= N; i++){
    dp[i][0] = dp[i-1][0] + dp[i-1][1];
    dp[i][1] = dp[i-1][0];
  }

  cout << dp[N][1] << endl;

  return 0;
}
