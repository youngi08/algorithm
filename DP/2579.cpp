#include <iostream>
#include <vector>

using namespace std;
vector<int> v;
int N;
int res=0;

void search(int floor, int check, int sum){

  if(floor == 1){
    res = sum;
    return;
  }

  if(check <3 && sum + v[floor-1] > sum + v[floor-2])
    search(floor-1, check++, sum+ v[floor]);
  else
    search(floor-2, 0, sum+ v[floor]);
}


int main()
{

  cin >> N;

  for(int i=1; i<=N; i++){
      int val;
      cin >> val;
      v.push_back(val);
  }

  search(v.size(), 0, v[v.size()]);

  cout << res << endl;

  return 0;
}
