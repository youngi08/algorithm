#include <iostream>
#include <algorithm>
using namespace std;

int dp[10001][3] = {0};
int wine[10001] = {0};

int main()
{
  int num;
  cin >> num;

  for(int i=1; i<num; i++){
    cin >> wine[i];

    int tmp = 0;
    tmp = max(dp[i][0], dp[i-1][1]);
    dp[i][0] = max(tmp, dp[i-1][2]);
    dp[i][1] = dp[i-1][0] + wine[i];
    dp[i][2] = dp[i-1][1] + wine[i];
  }

  cout << wine[num] << endl;

  return 0;
}





/*
//1차원 배열
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
  int num;
  cin >> num;

  vector<int> v;
  vector<int> dp(num+1, 0);

  for(int i=1; i<=num; i++){
    int wine;
    cin >> wine;
    v.push_back(wine);
  }

  dp[0] = 0;
  dp[1] = v[1];
  dp[2] = v[1] + v[2];

  for(int i=3; i<=num; i++){
    if(dp[i] < dp[i-2] + v[i]) dp[i] = dp[i-2] + v[i];
    if(dp[i] < dp[i-3] + v[i] + v[i-1]) dp[i] = dp[i-3] + v[i] + v[i-1];
  }

  cout << dp[num] << endl;
  //cout << res << endl;

  return 0;
}
*/
