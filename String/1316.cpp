//string
//1316.cpp 그룹단어 체크

#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
  int T;
  cin >> T;
  int res=0;

  for(int test_case; test_case<T; test_case++){
    string str;
    cin >> str;

    int count[26]={0};
    for(int i=1; i<str.size(); i++){
      if(count[str[i]-'a'] < 1){
        if(str[i-1] != str[i]){
          count[str[i-1]-'a']++;
        }
      }
      else{
        res++;
        break;
      }
    }
  }
  cout << T-res << endl;

  return 0;
}
