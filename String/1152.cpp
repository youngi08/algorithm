#include <iostream>
using namespace std;

int main()
{
  string str;
  getline(cin, str);

  int cnt=0;

  for(int i=0; i<str.length(); i++){
    if(str[i] == ' '){
      cnt++;
    }
  }

  if(str.at(0) == ' '){
    cnt--;
  }

  if(str.at(str.length()-1) == ' '){
    cnt--;
  }

  cout << cnt+1 << endl;
  return 0;
}
