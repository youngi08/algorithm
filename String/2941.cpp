//string
//2941.cpp 크로아티아 알파벳
#include <iostream>
using namespace std;

int main()
{
  string str;
  cin >> str;

  int count = 0;
  for(int i=str.size(); i > 0; i--){

    if(str[i] == '-'){
      if(str[i-1] == 'c' || str[i-1] == 'd') count++;
    }
    else if(str[i] == '='){
      if(str[i-1] == 'c' || str[i-1] == 's' || str[i-1] == 'z') count++;
      if(str[i-2] == 'd') count++;
    }
    else if(str[i] == 'j'){
      if(str[i-1] == 'l' || str[i-1] == 'n') count++;
    }
  }
  cout << str.size() - count << endl;

  return 0;
}


/*
#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

string ReplaceAll(string &str, const string& from, const string& to){
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != string::npos){
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
    return str;
}

int main()
{
  string chro[8] = {"c=", "c-", "dz=", "d-", "lj", "nj", "s=", "z="};
  string str;
  cin >> str;

  int count =0;

  for(int i=0; i<8; i++){
    str = ReplaceAll(str, chro[i], "1");
  }
  cout << str.size() << endl;

  return 0;
}
*/
