#include <iostream>
using namespace std;

char map[8][8] = {0};
int main()
{
  for(int i=0; i<8; i++){
    for(int j=0; j<8; j++){
      cin >> map[i][j];
    }
  }

  int res =0;
  for(int i=0; i<8; i++){
    for(int j=0; j<8; j++){
      if(map[i][j] == 'F'){
        if(i%2==0 && j%2 == 0) res++;
        if(i%2!=0 && j%2 != 0) res++;
      }
    }
  }

  cout << res<< endl;

  return 0;
}
