#include <iostream>
#include <vector>
#include <cstring>
using namespace std;

char map[50][50] = {0};

int main()
{
    int r, c;
    cin >> r >> c;

    for(int i=0; i<r; i++){
        for(int j=0; j<c; j++){
            cin >> map[i][j];
        }
    }

    typedef pair<int, int> boat;
    boat str[10];

    for(int i=0; i<r; i++){
        for(int j=0; j<c; j++){
            if(map[i][j] != '.' && map[i][j] != 'S' && map[i][j] != 'F'){
                int num=0;
                num = map[i][j] - '0';
                str[num].first = num;
                str[num].second = c-1-j;
            }
        }
    }

    for(int j=1; j<10; j++){
      for (int i = 1; i < 10; i++){
        if((str[i].second > str[i+1].second) && (i + 1 < 10)){
          swap(str[i], str[i + 1]);
        }
      }
    }

    int rank[10]={0};
    int cnt=0;

    for (int i = 1; i < 10; i++){
      if((str[i].second == str[i-1].second) && (i>0)){
        rank[str[i].first] = cnt;
      }
      else{
        rank[str[i].first] = ++cnt;
      }
    }

    for (int i = 1; i < 10; i++){
        cout <<rank[i]<< endl;
    }

    return 0;
}
