//1032.cpp 명령 프롬프트
#include <iostream>
#include <string>
using namespace std;

int main(int argc, const char * argv[]) {
    int num;

    cin >> num;

    string pattern, input;

    cin >> pattern;
    int len = pattern.length();

    for(int i=0; i<num-1; i++){
        cin >> input;

        for(int j=0; j<len; j++){
            if(pattern[j] != input[j]){
                pattern[j] = '?';
            }
        }
    }

    cout << pattern << endl;

    return 0;
}
