//1120 문자열
#include <iostream>
#include <cstring>
using namespace std;

int main()
{
  string A, B;
  cin >> A >> B;

  int Alen= A.length();
  int Blen= B.length();
  int compare = Blen - Alen;
  int min = Alen;

  for(int i = 0; i <= compare; i++){
    int check =0;
    for(int j =0; j < Alen; j++){
      if(A.at(j) - B.at(i+j) != 0){
        check++;
      }
    }
    if(check < min){
      min = check;
    }
  }

  cout << min << endl;

  return 0;
}
