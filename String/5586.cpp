#include <iostream>
using namespace std;

int main()
{
  string input;
  cin >> input;
  int joinum=0, ioinum=0;

  for(int i=0; i<input.length(); i++){
    if(input[i] == 'J' && input[i+1] == 'O' && input[i+2] == 'I'){
      joinum++;
    }
    else if(input[i] == 'I' && input[i+1] == 'O' && input[i+2] == 'I'){
      ioinum++;
    }
  }

  cout << joinum << endl;
  cout << ioinum << endl;

  return 0;
}
