#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

using namespace std;

int main()
{
  int num;
  vector<string> str;
  vector<string> rev;

  cin >> num;
  for(int i=0; i<num; i++){
    string input;
    cin >> input;
    str.push_back(input);
    reverse(input.begin(), input.end());
    rev.push_back(input);
  }

  string res;
  for(int i=0; i<num; i++){
    for(int j=0; j<num; j++){
      if(str[i] == rev[j]){
        res = str[i];
        break;
      }
    }
  }

  cout << res.size() << endl;
  cout << res[res.size()/2] << endl;

  return 0;
}
