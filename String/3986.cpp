#include <iostream>
#include <stack>

using namespace std;

int main()
{
  int num;
  cin >> num;

  int count = 0;
  for(int i=0; i<num; i++){
    string str;
    cin >> str;

    stack<char> st;
    for(int j=1; j<=str.size(); j++){

      st.push(str[i]);
      //cout << '\n';
      cout << st.top() <<" "<< str[j-1] << endl;

      if(st.top() == str[j-1]){
        st.pop();
      }
      else{
        st.push(str[j-1]);
      }
    }

    if(st.empty()) count++;
  }
  cout << count << endl;

  return 0;
}
