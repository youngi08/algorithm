//2857.cpp FBI
//문자열 처리

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
  vector<int> v;
  for(int i=0; i<5; i++){
    string input;
    cin >> input;

    if(input.find("FBI") != string::npos){
        v.push_back(i);
    }
  }

  sort(v.begin(), v.end());

  if(v.empty()){
    cout << "HE GOT AWAY!" << endl;
  }
  else{
    for(int i=0; i<v.size(); i++){
      cout << v[i]+1 <<" ";
    }
  }
  return 0;
}
