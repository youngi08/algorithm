//2810.cpp 컵홀더

#include <iostream>
using namespace std;

int main()
{
  int num;
  double star=1;
  cin >> num;

  string chair;
  cin >> chair;

  for(int i=0; i<num; i++){
    if(chair[i] == 'S'){
      star++;
    }
    if(chair[i] == 'L'){
      star += 0.5;
    }
  }

  if(star > num )
    cout << num << endl;
  else
    cout << (int) star << endl;

  return 0;
}
