#include <iostream>
using namespace std;

int main()
{
  string str;
  cin >> str;
  int n;
  int check[10] = {0};
  int max = check[0];

  for(int i=0; i<str.size(); i++){
    check[str.at(i)-48]++;
  }

  if((check[6] + check[9]) % 2 == 0){
    check[6] = (check[6] + check[9]) / 2;
    check[9] = 0;
  }else{
    check[6] = (check[6] + check[9]) / 2 + 1;
    check[9] = 0;
  }

  for(int j=0; j<10; j++){
    if(max < check[j]){
      max = check[j];
    }
  }

  cout << max;

  return 0;
}
