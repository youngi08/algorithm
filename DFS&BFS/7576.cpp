#include <iostream>
#include <queue>
using namespace std;

int n,m;
int box[1001][1001]={0};
int visited[1001][1001] = {0};
int dx[4] = {0,0,1,-1};
int dy[4] = {1,-1,0,0};

void printtest()
{
  for(int i=0; i<m; i++){
    for(int j=0; j<n; j++){
      cout << box[i][j] << " ";
    }
    cout <<'\n';
  }
  cout <<'\n';
}


void bfs(){

  queue < pair<int,int> > q;

  for(int i=0; i<m; i++){
    for(int j=0; j<n; j++){
      if(box[i][j] == 1){
        q.push(make_pair(i,j));
        box[i][j] = 0;
        visited[i][j] = 1;
      }
    }
  }

  while(!q.empty()){
    int x = q.front().first;
    int y = q.front().second;
    q.pop();

    for(int i=0 ;i<4; i++){
      int nx = x + dx[i];
      int ny = y + dy[i];

      if(nx>=0 && nx<m && ny>=0 && ny<n){
        if(box[nx][ny]==0 && visited[nx][ny] == 0){
          printtest();
          q.push(make_pair(nx,ny));
          visited[nx][ny] = 1;
          box[nx][ny] = box[x][y] + 1;
        }
      }
    }
  }
}

int check(){
  int ans = 0;

  for(int i=0; i<m; i++){
    for(int j=0 ;j<n; j++){
      if(ans < box[i][j]){
        ans = box[i][j];
      }

      if(visited[i][j] == 0 && box[i][j] == 0){
        return -1;
      }
    }
  }
  return ans;
}

int main(){
  cin >> n >> m;

  for(int i=0; i<m; i++)
    for(int j=0; j<n; j++)
      cin >> box[i][j];

    bfs();
    int day = check();
    cout << day << endl;

    return 0;
}
