#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int map[100][100];
int n, m; //n= x축 범위, m= y축 범위

bool inRange(int y,int x){
    return (y >= 0 && y < n && x >= 0 && x < m);
}

void dfs(int y, int x, int &cnt){
    int dx[4] = {-1, 0, 1, 0};
    int dy[4] = {0, -1, 0, 1};

    if (map[y][x] == 0) ++cnt;
    map[y][x] = 1;

    for (int i = 0; i < 4; i++){
        int nx = x + dx[i];
        int ny = y + dy[i];

        if (inRange(ny, nx) && map[ny][nx] == 0) dfs(ny, nx, cnt);
    }
    return;
}

int main(){
    int k;  //k = 직사각형 개수
    cin >> n >> m >> k;

    //좌표 x1~x2, y1~y2 해당하는 사각형 칠하기
    for (int i = 0; i < k; i++){
        int x1, y1, x2, y2;
        cin >> x1 >> y1 >> x2 >> y2;

        for(int k=y1; k< y2; k++){
          for(int h=x1; h < x2; h++){
            map[k][h] = 1;
          }
        }
    }

    vector<int> v;
    for (int i = 0; i < n; i++){
        for (int j = 0; j < m; j++){
            if (map[i][j] == 0){
                int cnt = 0;
                dfs(i, j, cnt);
                v.push_back(cnt);
            }
        }
    }

    cout << v.size() << '\n';
    sort(v.begin(),v.end());
    for (int i = 0;i < v.size(); i++){
      cout << v[i] << ' ';
    }
    cout <<'\n';

    return 0;
}
