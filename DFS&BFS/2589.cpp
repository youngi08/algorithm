#include <iostream>
#include <queue>

using namespace std;

int map[50][50]={0};
int visited[50][50]={0};
int X, Y;
int res=0;

bool inRange(int x, int y)
{
    return(x >= 0, x< X, y >= 0, y < Y);
}

void bfs(int x, int y)
{
  queue<pair<int, int>> q;

  visited[x][y] = 1;
  int dx[4] = {-1, 1, 0, 0};
  int dy[4] = {0, 0, -1, 1};

  q.push(make_pair(x, y));

	int count = 0;
	while (!q.empty()){
		int x = q.front().first;
		int y = q.front().second;
		q.pop();

    for(int i=0 ;i<4; i++){
      int nx = x + dx[i];
      int ny = y + dy[i];

			if(inRange(nx, ny) && visited[nx][ny] == 0 && map[nx][ny] == 'L'){
				q.push(make_pair(nx, ny));
				visited[nx][ny] = visited[x][y] + 1;
			}
		}
	}
}

int main()
{
  cin >> X >> Y;

  for(int i=0; i<X; i++){
    for(int j=0; j<Y; j++){
      cin >> map[i][j];
    }
  }

  for(int i=0; i<X; i++){
    for(int j=0; j<Y; j++){
      if(map[i][j] == 'L'){
        bfs(i, j);
      }
    }
  }

  //cout << res-1 << endl;

  return 0;
}
