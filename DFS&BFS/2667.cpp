//2667.cpp 단지번호붙히기
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int N;
int map[25][25]={0};
int visited[25][25] = {0};
int dx[4] = {-1, 0, 1, 0};
int dy[4] = {0, -1, 0, 1};
vector<int> group;
int home=0;

void bfs(int x, int y)
{
  visited[x][y] = 1;
  home++;
  //cout << "home:" << home << endl;

  for(int i=0; i<4; i++){
    int nx = x + dx[i];
    int ny = y + dy[i];
    cout << nx << ny << endl;

    if(nx >=0 && ny >=0; nx <N && ny <N){
      if(map[nx][ny] == 1 && visited[nx][ny] == 0){
        bfs(nx, ny);
      }
    }
  }
}


int main()
{
  cin >> N;

  for(int i=0; i < N ; i++){
		for(int j=0; j < N ; j++){
			scanf("%1d",&map[i][j]);

      if(map[i][j] == 1 && visited[i][j] == 0){
        home = 0;
        bfs(i,j);
        group.push_back(home);
      }
    }
  }

  sort(group.begin(), group.end());
  cout << group.size() << endl;
  for(int i=0; i < group.size(); i++){
    cout << group[i] << endl;
  }

}


/*
#include <iostream>
#include <queue>
#include <algorithm>
#include <vector>
using namespace std;

int map[25][25];
bool visited[25][25];
int dx[4] = {-1, 0, 1, 0};
int dy[4] = {0, -1, 0, 1};
int groupn=-1;
int num;

vector<int> housen;

void bfs(int x, int y)
{
   visited[x][y] = true;
   queue< pair<int, int> > q;
   q.push(make_pair(x,y));

   while(!q.empty()){
     int x = q.front().first;
     int y = q.front().second;

     q.pop();

     for(int i=0; i<4; i++){
       int nx = x+dx[i];
       int ny = y+dy[i];

       if(nx >=0 && ny >=0; nx <num && ny <num){
         if(map[nx][ny] != 0 && !visited[nx][ny]){
           housen[groupn]++;
           visited[nx][ny] = true;
           q.push(make_pair(nx, ny));
         }
       }
     }
   }
}

int main()
{
  cin >> num;

  for(int i=0; i<num; i++){
    for(int j=0; j<num; j++){
      cin >> map[i][j];
    }
  }

  for(int i=0; i<num; i++){
    for(int j=0; j<num; j++){
      if(map[i][j] != 0 && !visited[i][j]){
        groupn++;
        housen.push_back(1);
        visited[i][j] = true;
        bfs(i,j);
      }
    }
  }

  cout << groupn+1 << endl;

  sort(housen.begin(), housen.end());

  for(int i=0; i<=groupn; i++){
    cout << housen[i] << endl;
  }

  return 0;
}
*/
