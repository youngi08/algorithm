//DFS
//1987.cpp 알파벳

#include <iostream>
#include <algorithm>
using namespace std;

char map[21][21];
int alpha[26];
int R, C, res=0;

bool inRange(int x, int y){
  return(y >= 0 && y < C && x >= 0 && x < R);
}

void dfs (int x, int y, int lev){

  int dx[4] = {0, 0, -1, 1};
  int dy[4] = {-1, 1, 0, 0};

  alpha[map[x][y] - 'A'] = 1;
  //cout << "res =" << res <<" " << lev << endl;
  if(res < lev) res = lev;

  for(int i=0; i<4; i++){
    int nx = dx[i] + x;
    int ny = dy[i] + y;

    if(inRange(nx, ny) && alpha[map[nx][ny] - 'A'] == 0) dfs(nx, ny, lev+1);
  }

  alpha[map[x][y] - 'A'] = 0;
}


int main (void){
  cin >> R >> C;

  for(int i=0; i<R; i++){
    for(int j=0; j<C; j++){
      cin >> map[i][j];
    }
  }
  dfs(0, 0, 1);

  cout << res << endl;
  return 0;
}

/*
#include <iostream>
#include <string>

using namespace std;

string map[21];
int check[26];

int n, m;
int maxValue = 0;

bool inRange(int x, int y){
    return (y >= 0 x >= 0 && x < n && y < m );
}

void dfs(int x, int y, int cnt)
{
  int dx[4] = {-1, 0, 1, 0};
  int dy[4] = {0, -1, 0, 1};

    if (check[map[x][y] - 'A'] == 0){
        check[map[x][y] - 'A'] = 1;

        cnt > maxValue ? maxValue = cnt : 0;

        for (int i = 0; i < 4; i++){
            int nx = x + dx[i];
            int ny = y + dy[i];

            if (inRange(nx, ny)){ dfs(nx, ny, cnt+1);}
            check[map[x][y] - 'A'] = 0;
        }
    }
}

int main()
{
    int cnt = 1;

    cin >> n >> m;

    while (getchar() != '\n') {}

    for (int i = 0; i < n; i++)
        getline(cin, map[i]);

    dfs(0, 0, cnt);

    cout << maxValue;
}
*/
