#include <cstdio>
using namespace std;

int map[51][51];
int visited[51][51];

int dx[9] = {0,1,1,1,0,-1,-1,-1};
int dy[9] = {-1,-1,0,1,1,1,0,-1};
int W, H;

bool inRange(int x, int y)
{
  return(x>=0 && x<W && y>=0 && y<H);
}

void dfs(int y, int x)
{
  visited[y][x] = 1;

  for(int i = 0; i<9; i++){
    int nx = x +dx[i];
    int ny = y +dy[i];

    if(inRange(nx, ny)){
      if(map[ny][nx] == 1 && visited[ny][nx] == 0) dfs(ny,nx);
    }
  }
}

int main()
{
  while(1){
    scanf("%d %d",&W, &H);

    if(W == 0 && H == 0) break;

    for(int i = 0; i<H; ++i){
      for(int j = 0; j<W; ++j){
        scanf("%d", &map[i][j]);
        visited[i][j] = 0;
      }
    }

    int count = 0;

    for(int i = 0; i<H; i++){
      for(int j =0; j<W; j++){
        if(visited[i][j] == 0 && map[i][j] == 1){
          dfs(i,j);
          count++;
        }
      }
    }
  printf("%d\n", count);
  };
  return 0;
}
