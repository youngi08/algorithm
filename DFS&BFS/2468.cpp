//DFS
//2468.cpp 안전영역

#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;

int map[101][101];
int visited[101][101];
int dx[4] = {-1, 1, 0, 0};
int dy[4] = {0, 0, -1, 1};
int N;

bool inRange(int x, int y)
{
  return(x >= 0 && x < N && y >= 0 && y < N);
}

void dfs(int x, int y, int aux)
{
  visited[x][y] = 1;

  for(int i = 0; i < 4; i++){
    int nx = x + dx[i];
    int ny = y + dy[i];

    if(inRange(nx, ny) && map[x][y] >= aux && visited[nx][ny] == 0) dfs(nx, ny, aux);
  }
}

int main()
{
  cin >> N;

  int height = 0;

  for(int i = 0; i < N; i++){
    for(int j = 0; j < N; j++){
      cin >> map[i][j];

      height = max(height, map[i][j]);
    }
  }

  int cnt, maxCnt = 0;
  for(int k = 1; k <= height; k++){
    cnt = 0;
    memset(visited, 0, sizeof(visited));

    for(int i = 0; i < N; i++){
      for(int j = 0; j < N; j++){
        if(map[i][j]>=k && visited[i][j] == 0){
          cnt++;
          dfs(i, j, k);
        }
      }
    }
    maxCnt = max(maxCnt, cnt);
  }
  cout << maxCnt << endl;

  return 0;
}
