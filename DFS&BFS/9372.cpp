//9372.cpp 상근이의 여행
//brute force 방식
#include <iostream>
using namespace std;

int main()
{
  int T;
  cin >> T;

  for(int testcase=0; testcase<T; testcase++){
    int N, M;
    cin >> N >> M;

    int a , b;
    for(int i=0; i<M; i++){
      cin >> a >> b;
    }
    cout << N-1 << endl;
  }
  return 0;
}
