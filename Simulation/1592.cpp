//1592.cpp 영식이와 친구들
//유클리드 호제법, 시뮬레이션

#include <iostream>
using namespace std;

int check[1000] = {0};

int main()
{
  int N , M, L;
  int count=0;
  cin >> N >> M >> L;

  double term =1;
  check[(int)term]++;
  double num;

  while(1){
    count++;
    if(check[(int)term] < M){
      //cout << " term" << term <<" " << check[(int)term] << endl;

      num = (double)check[(int)term]/2;
    //  cout << "num" << num << endl;
      if(num == 1){  // 짝수면
        term -= 2;
        if(term < 0){
          term += (N);
          //cout <<"2"<< term << endl;
        }
        if(term == 0){
          term += N;
          //cout <<"3"<< term << endl;
        }
        check[(int)term]++;
      }
      else{ //홀수면
        term += 2;
        if(term > N){
          term -= N;
        }
        check[(int)term]++;
        //cout <<"1" <<term << endl;
      }
    }
    else{
      break;
    }
  }
  cout << count-1 << endl;

  return 0;
}
