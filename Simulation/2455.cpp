//2455.cpp 지능형 기차
//시뮬레이션

#include <iostream>
using namespace std;

int main()
{
  int max=0, temp=0;
  int in, out;

  for(int i=0; i<4; i++){
    cin >> in >> out;
    temp = temp + (out - in);

    if(temp > max){
      max = temp;
    }
  }

  cout << max;

  return 0;
}
