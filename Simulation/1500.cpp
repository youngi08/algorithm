#include <iostream>
using namespace std;

int main()
{
  int S, K;
  cin >> S >> K;

  int *res = new int[K];
  for(int i=0; i<K; i++){
    res[i] = S/K;
  }

  for(int i=0; i<S%K; i++){
    res[i]++;
  }

  long long int ans = 1;
  for(int i=0; i<K; i++){
    ans *= res[i];
  }

  cout << ans << endl;
  return 0;
}
