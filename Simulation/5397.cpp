//Simulation
//5397.cpp 키로거
#include <iostream>
#include <list>

using namespace std;

int main()
{
  int T;
  cin >> T;

  while(T--){
    list<char> lt;
    auto cursor = lt.end();

    string str;
    cin >> str;

    for(int i = 0; i<str.size(); i++) {
        if (str[i] == '-') {if(cursor != lt.begin()) cursor = lt.erase(--cursor); }
        else if (str[i] == '<') {if (cursor != lt.begin()) cursor--; }
        else if (str[i] == '>') {if (cursor != lt.end()) cursor++; }
        else cursor = lt.insert(cursor, str[i]), cursor++;
    }

    for (char c : lt) putchar(c);
    puts("");
  }

  return 0;
}

/*
#include <iostream>

using namespace std;

int main()
{
  int T;
  cin >> T;


  for(int i=0; i<T; i++){
    string str;
    cin >> str;

    int len = str.size();
    for(int i=0; i<len; i++){
      if(i+1 < str.size()){
        if(str[i] == '>'){
          swap(str[i+1], str[i-1]);
          str.erase(i, 1);
        }
        if(str[i] == '<'){
          swap(str[i+1], str[i-1]);
          str.erase(i, 1);
        }
        if(str[i] == '-'){
          str.erase(i, 1);
          str.erase(i-1, 1);
        }
      }
    }

    for(int j=0; j<len; j++){
      if(str[j] == '>'|| str[j] == '<' || str[j] == '-') str.erase(j, 1);
    }

    cout << str;
  }

  return 0;
}
*/
