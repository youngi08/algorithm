//2851.cpp 슈퍼 마리오
//구현

#include <iostream>
using namespace std;

int main()
{
  int sum=0;

  for(int i=0; i<10; i++){
    int num;
    cin >> num;

    if(sum+num >= 100){
      if(100 - sum >= sum+num-100){
        sum += num;
      }
      break;
    }
    sum+= num;
  }

  cout << sum << endl;

  return 0;
}
