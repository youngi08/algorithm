//3048 개미
#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
    typedef pair<string, int> ant;
    string str1, str2;
    ant str[50];

    int n1, n2, sec;

    cin >> n1 >> n2;
    cin >> str1 >> str2;
    cin >> sec;

    reverse(str1.begin(), str1.end());

    for (int i = 0; i < n1; i++){
        str[i].first = str1[i];
        str[i].second = 0;
    }

    int len = n1 + n2;
    for (int i = n1; i < len; i++){
        str[i].first = str2[i-n1];
        str[i].second = 1;
    }

    while (sec--){
        for (int i = 0; i < len; i++){
            if (i + 1 < len && str[i].second == 0 && str[i + 1].second == 1){
                swap(str[i], str[i + 1]);
                i++;
            }
        }
    }

    for (int i = 0; i < len; i++){
      cout << str[i].first;
    }

    return 0;
}
