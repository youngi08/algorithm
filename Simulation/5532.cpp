//5532.cpp 방학숙제
//시뮬레이션

//5532.cpp 방학숙제
//겨울 방학동안 숙제를 하지 않고 놀 수 있는 최대 날의 수를 구하는 프로그램을 작성하시오.

#include <iostream>
using namespace std;

int main()
{
  int L, A, B, C, D;  // 방학 총 L일 수학은 총 B페이지, 국어는 총 A페이지를 풀어야 한다.
                      // 하루에 국어를 최대 C페이지, 수학을 최대 D페이지 풀 수 있다.
  cin >> L;
  cin >> A;
  cin >> B;
  cin >> C;
  cin >> D;

  if(A%C !=0 && B%D !=0){
    if((A/C) > (B/D)){
      L -= (A/C)+1;
    }
    else{
      L -= (B/D)+1;
    }
  }
  else{
    if((A/C) > (B/D)){
      L -= (A/C);
    }
    else{
      L -= (B/D);
    }
  }
  cout << L;

  return 0;
}
