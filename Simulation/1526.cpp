//simulation
//1526.cpp 가장 큰 금민수
#include <iostream>
#include <string> //c++11

using namespace std;

int main()
{
  string str;
  cin >> str;

  int N = stoi(str);
  string res;
  while(1){
    str = to_string(N);
    int four=0, seven = 0;

    for(int i=0; i<str.size(); i++){
      if(str[i] == '7') ++seven;
      else if(str[i] == '4') ++four;
    }
    if((seven+four) == str.size()){
      res = str;
      break;
    }
    N--;
  }

  cout << res << endl;

  return 0;
}
