#include <iostream>

using namespace std;

class Car
{
  public:
    int speed;  //속도
    int gear; //주행 거리
    int color;  //색상

    void SetGear(int newGear);
    void speedUp(int increment);
    void speedDown(int decrement);
}

Car::setGear(int newGear) { // 기어 설정 멤버함수
  gear = newGear;
}
Car::speedUp(int increment) { // 속도 증가 멤버함수
  speed += increment;
}
Car::speedDown(int decrement) { // 속도 감소 멤버함수
  speed -= decrement;
}

// Car 클래스를 상속 받는 SportsCar 클래스
class SportsCar : public Car {
  // 1개의 멤버변수를 추가
  bool turbo;

  public:
    // 1개의 멤버함수를 추가
    void setTurbo(bool newValue) { // 터보 모드 설정 멤버함수
      turbo = newValue;
    }
};
