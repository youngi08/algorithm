#include <iostream>

using namespace std;

class Point
{
  public: // 접근 제어와 관련된 키워드

    //멤버 함수
    void Print();

    //생성자
    Point();
    Point(int initX, int initY);
    Point(const Point& pt);

    //접근자
    //멤버변수 x, y의 값을 읽거나 변경할 수 있는 접근자 추가
    void SetX(int value){ x = value;};
    void SetY(int value){ y = value;};
    int GetX(){return x;};
    int GetY(){return y;};

  private:
    //멤버 변수
    int x, y;

};

Point::Point()
{
    x = 0;
    y = 0;
}

Point::Point(int initX, int initY)
{
  x = initX;
  y = initY;
}

Point::Point(const Point& pt)
{
  x = pt.x;
  y = pt.y;
}

void Point::Print()
{
  cout << "(" << x << "," << y << ")\n";
}


int main()
{
  Point pt;

  //접근자를 이용해서 멤버 변수의 값을 바꾼다
  pt.SetX(100);
  pt.SetY(100);

  cout << "pt.GetX = " << pt.GetX() << endl;
  cout << "pt.GetY = " << pt.GetY() << endl;

  /*
  Point pt1, pt2;

  pt1.x = 100;
  pt1.y = 100;
  pt2.x = 200;
  pt2.y = 200;

  pt1.Print();
  pt2.Print();
  */

  return 0;
}
