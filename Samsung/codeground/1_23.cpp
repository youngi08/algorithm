#include <iostream>
#include <vector>
#include <algorithm>
#define max(a,b) ((a)>(b)?(a):(b))

using namespace std;

int Answer;

int main(int argc, char** argv)
{
	int T, test_case;

	// freopen("input.txt", "r", stdin);
	cin >> T;
	for(test_case = 0; test_case  < T; test_case++)
	{
		Answer = 0;
    int num = 0;
    cin >> num;

    int ** sum = new int*[num];
    for(int i = 0; i < num; i++)
      sum[i] = new int[num*num];

    vector<int> v;
    for(int i=0; i<num; i++){
      int tmp=0;
      for(int j=0; j<num; j++){
        cin >> sum[i][j];
        tmp += sum[i][j];
      }
      v.push_back(tmp);
		}

		int res=0;
    for(int k=0; k<v.size(); k++){
			int s =0, z=0, plus=0;
    	for(int h=k; h<v.size(); h++){
				for(int p =0; p<v.size(); p++){
					s += v[p];
					res = max(res, s);

					plus = v[p] - sum[p][k];
					//cout << "res=" << plus << " " << v[p]<< endl;
					//cout << res << endl;
				}
      }
			z = max(plus, res);
			Answer = max(Answer, z);
    }

		// Print the answer to standard output(screen).
		cout << "Case #" << test_case+1 << endl;
		cout << Answer << endl;
	}

	return 0;//Your program should return 0 on normal termination.
}
