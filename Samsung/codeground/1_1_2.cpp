//연습문제 1_1
//숫자 골라내기
//다른 풀이법 (XOR 특징 활용)

/*
XOR의 성질을 이해하면 쉽게 풀 수 있는 문제입니다.
그것은 바로 임의의 수 n에 대해 n⊕n=0 이라는 점입니다.
XOR은 비트의 수가 다를 때 1이므로, 같은 숫자와의 XOR 연산결과는 당연히 0이 되겠죠.
그러므로 입력받은 모든 수를 XOR 연산한 결과를 출력하면 됩니다.
*/

//두 값의 각 자릿수를 비교해, 값이 같으면 0, 다르면 1을 계산한다.

#include <iostream>
using namespace std;

int Answer;

int main(int argc, char** argv)
{
	int T, test_case;

	cin >> T;
	for(test_case = 0; test_case  < T; test_case++)
	{
		Answer = 0;

		int input;
		cin >> input;
		int num;

		for(int i=0; i<input; i++){
	       cin >> num;
	       Answer ^= num;
		}

		cout << "Case #" << test_case+1 << endl;
		cout << Answer << endl;
	}

	return 0;
}
