#include <iostream>
#include <vector>
using namespace std;

int main(int argc, char** argv)
{
	int T, test_case;
	cin >> T;
	for(test_case = 0; test_case  < T; test_case++)
	{
    int N, M, K;  //N = 인원수, M = 커피 종류, K = 법인 카드 최대 금액
    int c_i; // i번째 사람이 좋아하는 커피의 종류
    int p_i; // 커피의 가격?

    cin >> N >> M >> K;

    vector<int> sel;
    vector<int> cost;

    for(int i=0; i<N; i++){
      cin >> c_i;
      sel.push_back(c_i);
    }

    for(int j=0; j<M; j++){
      cin >> p_i;
      cost.push_back(p_i);
    }

    for(int k=0; k<sel.size(); k++){
      K -= cost[sel[k]-1];
    }

		cout << "Case #" << test_case+1 << endl;
    if(K >= 0) cout << "Y" << endl;
    else cout << "N" << endl;
	}

	return 0;
}
