#include <iostream>

using namespace std;

int Answer;

int main(int argc, char** argv)
{
	int T, test_case;

	cin >> T;
	for(test_case = 0; test_case  < T; test_case++)
	{

		Answer = 0;

    int A, B, D;  //1분마다 +A -B이동 학교-집 거리 D
    cin >> A >> B >> D;
    unsigned long long t=1;


    while(1){
      //cout << t << D << A << B << endl;
      if(D<=1) break;
      ++t;
      D = D-A;
      if(D<=1) break;
      //++t;
      D += B;
    }

		// Print the answer to standard output(screen).
		cout << "Case #" << test_case+1 << endl;
		cout << t << endl;
	}

	return 0;
}
