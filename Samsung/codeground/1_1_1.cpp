//연습문제 1_1
//숫자 골라내기

#include <iostream>
#define MAX 300
using namespace std;

int Answer;

int main(int argc, char** argv)
{
	int T, test_case;

	cin >> T;
	for(test_case = 0; test_case  < T; test_case++)
	{
		Answer = 0;

		int input;
    int * num;
    int * tmp;
		cin >> input;
    num = new int[input];
    tmp = new int[input];

		for(int i=0; i<input; i++){
      int val;
      cin >> val;
      num[i] = val;
			tmp[i]++;
    }

    for(int i=0; i<input; i++){
      if(tmp[i] %2 != 0){
	       Answer ^= num[i];
      }
    }

		cout << "Case #" << test_case+1 << endl;
		cout << Answer << endl;
    delete num;
    delete tmp;
	}

	return 0;
}
