
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int Answer;

class list {
public:
	int x, y, cost;
	bool operator()(list i, list j) { return i.cost < j.cost; }
};

int disjoint(int x, vector<int> &uf) {
	return (x != uf[x]) ? uf[x] = disjoint(uf[x], uf) : x;
}

int main(int argc, char** argv)
{
	int T, test_case;

	// freopen("input.txt", "r", stdin);

	cin >> T;
	for(test_case = 0; test_case  < T; test_case++)
	{
		Answer = 0;
    int N=0, M=0; //N= 그래프 정점의 개수, M = 간선의 개수
    cin >> N >> M;

    vector <int> uf(N + 1);
	  vector <list> v(M);

    for (int i = 1; i <= N; i++) uf[i] = i;

    for(int i=0; i<M; i++){
        cin >> v[i].x >> v[i].y >> v[i].cost;
    }

    sort(v.begin(), v.end(), list());

    int ans = 0;

    vector<int> res;

    for (int i = 0; i < M; i++){
  		if (disjoint(v[i].y, uf) != disjoint(v[i].x, uf)) {
  			uf[disjoint(v[i].y, uf)] = uf[v[i].x];
  			ans = v[i].cost;
        res.push_back(ans);
        //cout << "ans=" << ans<<endl;
      }
    }

    if(res.size() > 1){
      Answer = res[res.size()/2];
    }
    else
      Answer = res[0];
		// Print the answer to standard output(screen).
		cout << "Case #" << test_case+1 << endl;
		cout << Answer << endl;
	}

	return 0;//Your program should return 0 on normal termination.
}
