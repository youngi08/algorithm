#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int Answer;

int main(int argc, char** argv)
{
	int T, test_case;

  //freopen("hinput.txt", "r", stdin);

	cin >> T;
	for(test_case = 0; test_case  < T; test_case++)
	{
		Answer = 0;
    int alpha[4] = {0};
    string str;
    cin >> str;

    for(int i=0; i<str.size(); i++){
      if(str[i] == 'h') alpha[0]++;
      else if(str[i] == 'e') alpha[1]++;
      else if(str[i] == 'l') alpha[2]++;
      else if(str[i] == 'o') alpha[3]++;
    }

  int tmp = 0;
  tmp = min(alpha[0], alpha[1]);
  tmp = min(tmp, alpha[3]);

  alpha[2] = alpha[2]/2;

  if(tmp >= alpha[2]) Answer = alpha[2];
  else Answer = tmp;

	cout << "Case #" << test_case+1 << endl;
	cout << Answer << endl;
	}

	return 0;
}
