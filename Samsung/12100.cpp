//12100.cpp 2048(easy)

#include <iostream>
#include <algorithm>
#include <queue>
#include <vector>
using namespace std;

int bfs() {
    queue<vector<vector<int>>> qu;
    qu.push(vt);
    int ret = 0, c = 0;

    while (qu.size()) {
        int qs = qu.size();
        while (qs--) {
            vector<vector<int>> v = qu.front();
            vector<vector<int>> visited(n);
            qu.pop();

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++)
                    ret = max(ret, v[i][j]);
            }
            for (int i = 0; i < n; i++)
                visited[i].assign(n, 0);

            //위로 탐색
            vector<vector<int>> up = v;
            for (int i = 1; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    int cx = i;
                    while (cx - 1 >= 0 && up[cx - 1][j] == 0)
                        cx--;
                    if (cx - 1 >= 0 && up[cx - 1][j] == up[i][j] && !visited[cx - 1][j]) {
                        up[cx - 1][j] = up[i][j] * 2;
                        up[i][j] = 0;
                        visited[cx - 1][j] = 1;
                    }
                    else
                        swap(up[cx][j], up[i][j]);
                }
            }
            for (int i = 0; i < n; i++)
                visited[i].assign(n, 0);
            vector<vector<int>> down = v;

            for (int i = n - 2; i >= 0; i--) {
                for (int j = 0; j < n; j++) {
                    int cx = i;
                    while (cx + 1 < n&&down[cx + 1][j] == 0)
                        cx++;
                    if (cx + 1 < n&&down[cx + 1][j] == down[i][j] && !visited[cx + 1][j]) {
                        down[cx + 1][j] = down[i][j] * 2;
                        down[i][j] = 0;
                        visited[cx + 1][j] = 1;
                    }
                    else
                        swap(down[cx][j], down[i][j]);
                }
            }
            for (int i = 0; i < n; i++)
                visited[i].assign(n, 0);
            vector<vector<int>> left = v;
            for (int i = 0; i < n; i++) {
                for (int j = 1; j < n; j++) {
                    int cy = j;
                    while (cy - 1 >= 0 && left[i][cy - 1] == 0)
                        cy--;
                    if (cy - 1 >= 0 && left[i][cy - 1] == left[i][j] && !visited[i][cy - 1]) {
                        left[i][cy - 1] = left[i][j] * 2;
                        left[i][j] = 0;
                        visited[i][cy - 1] = 1;
                    }
                    else
                        swap(left[i][cy], left[i][j]);
                }
            }
            for (int i = 0; i < n; i++)
                visited[i].assign(n, 0);
            vector<vector<int>> right = v;
            for (int i = 0; i < n; i++) {
                for (int j = n - 2; j >= 0; j--) {
                    int cy = j;
                    while (cy + 1 < n&&right[i][cy + 1] == 0)
                        cy++;
                    if (cy + 1 < n&&right[i][cy + 1] == right[i][j] && !visited[i][cy + 1]) {
                        right[i][cy + 1] = right[i][j] * 2;
                        right[i][j] = 0;
                        visited[i][cy + 1] = 1;
                    }
                    else
                        swap(right[i][cy], right[i][j]);
                }
            }
            qu.push(up);
            qu.push(down);
            qu.push(left);
            qu.push(right);
        }
        if (c == 5)
            break;
        c++;
    }
    return ret;
}

int main()
{
  int n, x;
  vector<vector<int>> vt;

  cin >> n;
  vt.resize(n);

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      cin >> x;
      vt[i].push_back(x);
    }
  }
  cout << bfs() << endl;

  return 0;
}
