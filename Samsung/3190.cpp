//3190.cpp 뱀

#include <iostream>
using namespace std;
#define APPLE 1;
#define SNAKE 2;
#define RIGHT 3;
#define LEFT 4;

struct Sinfo{
  int sec;  // 뱀 위치 변경 시간
  string position; // 뱀 방향 변경 위치
};

int map[100][100]={0};
int direct[100][100]={0}; // 2: 오른쪽, 3: 왼쪽

int drow[4] = {0, -1, 0, 1};
int dcol[4] = {-1, 0, 1, 0};

int main()
{
  int N;  //맵의 크기 N*N
  int K;  // 사과의 갯수
  int L; // 뱀의 방향 변경 정보 갯수
  int nowpos = RIGHT; //뱀의 현재 방향 정보
  int hrow=1, hcol=1; //뱀의 머리위치
  int trow=1, tcol=1; //뱀의 꼬리위치

  cin >> N;
  cin >> K;

  //맵에 사과 위치 표시
  for(int i=0; i< K; i++){
    int ax, ay; // 사과의 위치
    cin >> ax >> ay;
    map[ax][ay] = APPLE;
  }

  cin >> L;
  struct Sinfo info[L];

  //뱀위 방향 변경 정보 입력
  for(int i=0; i<L; i++){
    cin >> info[i].sec >> info[i].position;
  }

  //뱀의 초기 위치
  direct[hrow][hcol] = nowpos;
  map[hrow][hcol] = SNAKE;

  //뱀 이동
  for(int i=0; i<L; i++){
    int step =0;
    int dsec = info[i].sec;
    char dpos = info[i].position;

    int diff = dsec;
    if(i > 0)
      diff = info[i].time - info[i - 1].time;

    while(diff-- >0){
      step++;
      // 머리 이동
      hrow += drow[nowpos];
      hcol += dcol[nowpos];

      // 종료 - 벽에 충돌한 경우
      if (hrow < 1 || hrow > N || hcol < 1 || hcol > N) {
        cout << step << endl;
        break;
      }

      //진행 - 빈칸인 경우
      if (map[hrow][hcol] == 0) {
        // 맵업데이트
        map[hrow][hcol] = SNAKE;
        direct[hrow][hcol] = nowpos;
        map[trow][tcol] = 0;

        // 꼬리 이동
        int tailDir = direct[tailRow][tailCol];
        tailRow += dRow[tailDir];
        tailCol += dCol[tailDir];
      }

      // 진행 - 사과를 만난 경우
      else if (map[hrow][hcol] == APPLE) {
        // 맵 업데이트
        map[hrow][hcol] = SNAKE;
        direct[hrow][hcol] = nowpos;
      }

      // 종료 - 자기 자신을 만난 경우
      else if (map[hrow][hcol] == SNAKE) {
        cout << step << endl;
        break;
      }
    }
    nowpos = C == 'L' ? (nowpos - 1 + 4) % 4 : (nowpos + 1) % 4;

  }

  return 0;
}
