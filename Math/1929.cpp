//1929.cpp 소수구하기
#include <cstdio>
#include <vector>
#define MAX 1000000
using namespace std;

int main(){
    int N, M;

    vector<bool> PrimeArray(1000001, 1);

    PrimeArray[0] = PrimeArray[1] = false;

    for(int i=2; (i*i)<=MAX; i++){
        if(PrimeArray[i]){
            for(int j=i*i; j<=MAX; j+=i)
                PrimeArray[j]=false;
        }
    }

    scanf("%d %d", &N, &M);

    for(int i = N; i<=M; i++){
        if(PrimeArray[i]){
            printf("%d\n", i);
        }
    }
    return 0;
}
