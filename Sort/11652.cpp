#include <iostream>
#include <map>
using namespace std;

map<long long, int> mp;
int main()
{
  int num;
  cin >> num;

  for(int i=0; i<num; i++){
    long long int tmp;
    cin >> tmp;
    mp[tmp]++;
  }

  long long int res=0, count=0;
  for (auto &it : mp) if (it.second > count) res = it.first, count = it.second;

  cout << res << endl;

  return 0;
}
