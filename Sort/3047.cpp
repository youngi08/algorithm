//3047.cpp ABC
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
    vector<int> v;
    string alpa;
    int input;
    int a, b, c;

    for(int i=0; i<3; i++){
      cin >> input;
      v.push_back(input);
    }

    sort(v.begin(), v.end());

    a = v[0];
    b = v[1];
    c = v[2];

    cin >> alpa;

    for(int i=0; i<alpa.length(); i++){
      if(alpa[i] == 'A'){
        cout << a << " ";
      }
      if(alpa[i] == 'B'){
        cout << b << " ";
      }
      if(alpa[i] == 'C'){
        cout << c << " ";
      }
    }
    cout << endl;

    return 0;
}
