#include <iostream>
#include <string>
#include <set>
#include <algorithm>
using namespace std;
set <string> list;

int main()
{
    int num;
    cin >> num;

    for(int i=0; i<num; i++){
      string input, state;
      cin >> input >> state;

      if(state.compare("leave") == 0){
        list.erase(input);
      }
      else{
        list.insert(input);
      }
    }

    set< string >::reverse_iterator itor = list.rbegin();
	  while (itor != list.rend()){
		cout << *itor << endl;
		itor++;
	}


  return 0;
}
